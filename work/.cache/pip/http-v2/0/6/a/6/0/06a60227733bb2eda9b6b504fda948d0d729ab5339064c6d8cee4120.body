Metadata-Version: 2.1
Name: tiled
Version: 0.1.0a107
Summary: Structured Scientific Data Access Service
Project-URL: Homepage, https://github.com/bluesky/tiled
Project-URL: Documentation, https://blueskyproject.io/tiled
Project-URL: Bug Tracker, https://github.com/bluesky/tiled/issues
Author-email: Bluesky Project Contributors <dallan@bnl.gov>
Maintainer-email: Brookhaven National Laboratory <dallan@bnl.gov>
License-File: LICENSE
Classifier: Development Status :: 4 - Beta
Classifier: License :: OSI Approved :: BSD License
Classifier: Programming Language :: Python :: 3 :: Only
Classifier: Programming Language :: Python :: 3.7
Classifier: Programming Language :: Python :: 3.8
Classifier: Programming Language :: Python :: 3.9
Classifier: Programming Language :: Python :: 3.10
Classifier: Programming Language :: Python :: 3.11
Classifier: Topic :: Scientific/Engineering :: Physics
Requires-Python: >=3.8
Provides-Extra: all
Requires-Dist: aiofiles; extra == 'all'
Requires-Dist: aiosqlite; extra == 'all'
Requires-Dist: alembic; extra == 'all'
Requires-Dist: anyio; extra == 'all'
Requires-Dist: appdirs; extra == 'all'
Requires-Dist: asyncpg; extra == 'all'
Requires-Dist: awkward>=2.4.3; extra == 'all'
Requires-Dist: blosc; extra == 'all'
Requires-Dist: cachetools; extra == 'all'
Requires-Dist: cachey; extra == 'all'
Requires-Dist: click!=8.1.0; extra == 'all'
Requires-Dist: dask; extra == 'all'
Requires-Dist: dask[array]; extra == 'all'
Requires-Dist: dask[dataframe]; extra == 'all'
Requires-Dist: entrypoints; extra == 'all'
Requires-Dist: fastapi; extra == 'all'
Requires-Dist: h5netcdf; extra == 'all'
Requires-Dist: h5py; extra == 'all'
Requires-Dist: httpx!=0.23.1,>=0.20.0; extra == 'all'
Requires-Dist: jinja2; extra == 'all'
Requires-Dist: jmespath; extra == 'all'
Requires-Dist: jsonschema; extra == 'all'
Requires-Dist: lz4; extra == 'all'
Requires-Dist: msgpack>=1.0.0; extra == 'all'
Requires-Dist: ndindex; extra == 'all'
Requires-Dist: numpy; extra == 'all'
Requires-Dist: openpyxl; extra == 'all'
Requires-Dist: orjson; extra == 'all'
Requires-Dist: packaging; extra == 'all'
Requires-Dist: pandas; extra == 'all'
Requires-Dist: parquet; extra == 'all'
Requires-Dist: pillow; extra == 'all'
Requires-Dist: prometheus-client; extra == 'all'
Requires-Dist: psutil; extra == 'all'
Requires-Dist: pyarrow; extra == 'all'
Requires-Dist: pydantic<2,>=1.8.2; extra == 'all'
Requires-Dist: python-dateutil; extra == 'all'
Requires-Dist: python-jose[cryptography]; extra == 'all'
Requires-Dist: python-multipart; extra == 'all'
Requires-Dist: pyyaml; extra == 'all'
Requires-Dist: sparse; extra == 'all'
Requires-Dist: sqlalchemy[asyncio]>=2; extra == 'all'
Requires-Dist: starlette; extra == 'all'
Requires-Dist: tifffile; extra == 'all'
Requires-Dist: toolz; extra == 'all'
Requires-Dist: typer; extra == 'all'
Requires-Dist: uvicorn[standard]; extra == 'all'
Requires-Dist: watchgod; extra == 'all'
Requires-Dist: xarray; extra == 'all'
Requires-Dist: zarr; extra == 'all'
Requires-Dist: zstandard; extra == 'all'
Provides-Extra: array
Requires-Dist: dask[array]; extra == 'array'
Requires-Dist: numpy; extra == 'array'
Provides-Extra: client
Requires-Dist: appdirs; extra == 'client'
Requires-Dist: awkward>=2.4.3; extra == 'client'
Requires-Dist: blosc; extra == 'client'
Requires-Dist: click!=8.1.0; extra == 'client'
Requires-Dist: dask[array]; extra == 'client'
Requires-Dist: dask[dataframe]; extra == 'client'
Requires-Dist: entrypoints; extra == 'client'
Requires-Dist: httpx!=0.23.1,>=0.20.0; extra == 'client'
Requires-Dist: jsonschema; extra == 'client'
Requires-Dist: lz4; extra == 'client'
Requires-Dist: msgpack>=1.0.0; extra == 'client'
Requires-Dist: ndindex; extra == 'client'
Requires-Dist: numpy; extra == 'client'
Requires-Dist: pandas; extra == 'client'
Requires-Dist: pyarrow; extra == 'client'
Requires-Dist: pyyaml; extra == 'client'
Requires-Dist: sparse; extra == 'client'
Requires-Dist: typer; extra == 'client'
Requires-Dist: xarray; extra == 'client'
Requires-Dist: zstandard; extra == 'client'
Provides-Extra: compression
Requires-Dist: blosc; extra == 'compression'
Requires-Dist: lz4; extra == 'compression'
Requires-Dist: zstandard; extra == 'compression'
Provides-Extra: dataframe
Requires-Dist: dask[dataframe]; extra == 'dataframe'
Requires-Dist: pandas; extra == 'dataframe'
Requires-Dist: pyarrow; extra == 'dataframe'
Provides-Extra: dev
Requires-Dist: coverage; extra == 'dev'
Requires-Dist: flake8; extra == 'dev'
Requires-Dist: ipython; extra == 'dev'
Requires-Dist: ldap3; extra == 'dev'
Requires-Dist: matplotlib; extra == 'dev'
Requires-Dist: mistune<2.0.0; extra == 'dev'
Requires-Dist: myst-parser; extra == 'dev'
Requires-Dist: numpydoc; extra == 'dev'
Requires-Dist: pre-commit; extra == 'dev'
Requires-Dist: pytest; extra == 'dev'
Requires-Dist: pytest-asyncio; extra == 'dev'
Requires-Dist: pytest-rerunfailures; extra == 'dev'
Requires-Dist: sphinx!=4.1.0,!=4.1.1,!=4.1.2,!=4.2.0; extra == 'dev'
Requires-Dist: sphinx-click; extra == 'dev'
Requires-Dist: sphinx-copybutton; extra == 'dev'
Requires-Dist: sphinx-rtd-theme; extra == 'dev'
Provides-Extra: formats
Requires-Dist: h5netcdf; extra == 'formats'
Requires-Dist: h5py; extra == 'formats'
Requires-Dist: openpyxl; extra == 'formats'
Requires-Dist: pillow; extra == 'formats'
Requires-Dist: tifffile; extra == 'formats'
Provides-Extra: minimal-client
Requires-Dist: appdirs; extra == 'minimal-client'
Requires-Dist: click!=8.1.0; extra == 'minimal-client'
Requires-Dist: entrypoints; extra == 'minimal-client'
Requires-Dist: httpx!=0.23.1,>=0.20.0; extra == 'minimal-client'
Requires-Dist: jsonschema; extra == 'minimal-client'
Requires-Dist: msgpack>=1.0.0; extra == 'minimal-client'
Requires-Dist: pyyaml; extra == 'minimal-client'
Requires-Dist: typer; extra == 'minimal-client'
Provides-Extra: minimal-server
Requires-Dist: aiofiles; extra == 'minimal-server'
Requires-Dist: aiosqlite; extra == 'minimal-server'
Requires-Dist: alembic; extra == 'minimal-server'
Requires-Dist: anyio; extra == 'minimal-server'
Requires-Dist: appdirs; extra == 'minimal-server'
Requires-Dist: cachetools; extra == 'minimal-server'
Requires-Dist: cachey; extra == 'minimal-server'
Requires-Dist: click!=8.1.0; extra == 'minimal-server'
Requires-Dist: dask; extra == 'minimal-server'
Requires-Dist: fastapi; extra == 'minimal-server'
Requires-Dist: httpx!=0.23.1,>=0.20.0; extra == 'minimal-server'
Requires-Dist: jinja2; extra == 'minimal-server'
Requires-Dist: jmespath; extra == 'minimal-server'
Requires-Dist: jsonschema; extra == 'minimal-server'
Requires-Dist: msgpack>=1.0.0; extra == 'minimal-server'
Requires-Dist: orjson; extra == 'minimal-server'
Requires-Dist: packaging; extra == 'minimal-server'
Requires-Dist: parquet; extra == 'minimal-server'
Requires-Dist: prometheus-client; extra == 'minimal-server'
Requires-Dist: psutil; extra == 'minimal-server'
Requires-Dist: pydantic<2,>=1.8.2; extra == 'minimal-server'
Requires-Dist: python-dateutil; extra == 'minimal-server'
Requires-Dist: python-jose[cryptography]; extra == 'minimal-server'
Requires-Dist: python-multipart; extra == 'minimal-server'
Requires-Dist: pyyaml; extra == 'minimal-server'
Requires-Dist: sqlalchemy[asyncio]>=2; extra == 'minimal-server'
Requires-Dist: starlette; extra == 'minimal-server'
Requires-Dist: toolz; extra == 'minimal-server'
Requires-Dist: typer; extra == 'minimal-server'
Requires-Dist: uvicorn[standard]; extra == 'minimal-server'
Requires-Dist: watchgod; extra == 'minimal-server'
Requires-Dist: zarr; extra == 'minimal-server'
Provides-Extra: server
Requires-Dist: aiofiles; extra == 'server'
Requires-Dist: aiosqlite; extra == 'server'
Requires-Dist: alembic; extra == 'server'
Requires-Dist: anyio; extra == 'server'
Requires-Dist: appdirs; extra == 'server'
Requires-Dist: asyncpg; extra == 'server'
Requires-Dist: awkward>=2.4.3; extra == 'server'
Requires-Dist: blosc; extra == 'server'
Requires-Dist: cachetools; extra == 'server'
Requires-Dist: cachey; extra == 'server'
Requires-Dist: click!=8.1.0; extra == 'server'
Requires-Dist: dask; extra == 'server'
Requires-Dist: dask[array]; extra == 'server'
Requires-Dist: dask[dataframe]; extra == 'server'
Requires-Dist: fastapi; extra == 'server'
Requires-Dist: h5netcdf; extra == 'server'
Requires-Dist: h5py; extra == 'server'
Requires-Dist: httpx!=0.23.1,>=0.20.0; extra == 'server'
Requires-Dist: jinja2; extra == 'server'
Requires-Dist: jmespath; extra == 'server'
Requires-Dist: jsonschema; extra == 'server'
Requires-Dist: lz4; extra == 'server'
Requires-Dist: msgpack>=1.0.0; extra == 'server'
Requires-Dist: ndindex; extra == 'server'
Requires-Dist: numpy; extra == 'server'
Requires-Dist: openpyxl; extra == 'server'
Requires-Dist: orjson; extra == 'server'
Requires-Dist: packaging; extra == 'server'
Requires-Dist: pandas; extra == 'server'
Requires-Dist: parquet; extra == 'server'
Requires-Dist: pillow; extra == 'server'
Requires-Dist: prometheus-client; extra == 'server'
Requires-Dist: psutil; extra == 'server'
Requires-Dist: pyarrow; extra == 'server'
Requires-Dist: pydantic<2,>=1.8.2; extra == 'server'
Requires-Dist: python-dateutil; extra == 'server'
Requires-Dist: python-jose[cryptography]; extra == 'server'
Requires-Dist: python-multipart; extra == 'server'
Requires-Dist: pyyaml; extra == 'server'
Requires-Dist: sparse; extra == 'server'
Requires-Dist: sqlalchemy[asyncio]>=2; extra == 'server'
Requires-Dist: starlette; extra == 'server'
Requires-Dist: tifffile; extra == 'server'
Requires-Dist: toolz; extra == 'server'
Requires-Dist: typer; extra == 'server'
Requires-Dist: uvicorn[standard]; extra == 'server'
Requires-Dist: watchgod; extra == 'server'
Requires-Dist: xarray; extra == 'server'
Requires-Dist: zarr; extra == 'server'
Requires-Dist: zstandard; extra == 'server'
Provides-Extra: sparse
Requires-Dist: ndindex; extra == 'sparse'
Requires-Dist: pyarrow; extra == 'sparse'
Requires-Dist: sparse; extra == 'sparse'
Provides-Extra: xarray
Requires-Dist: dask[array]; extra == 'xarray'
Requires-Dist: pandas; extra == 'xarray'
Requires-Dist: pyarrow; extra == 'xarray'
Requires-Dist: xarray; extra == 'xarray'
Description-Content-Type: text/markdown

# Tiled

Tiled is a **data access** service for data-aware portals and data science tools.
Tiled has a Python client and integrates naturally with Python data science
libraries, but nothing about the service is Python-specific; it also works from
a web browser or any Internet-connected program.

Tiled’s service can sit atop databases, filesystems, and/or remote
services to enable **search** and **structured, chunkwise access to data** in an
extensible variety of appropriate formats, providing data in a consistent
structure regardless of the format the data happens to be stored in at rest. The
natively-supported formats span slow but widespread interchange formats (e.g.
CSV, JSON) and fast, efficient ones (e.g. C buffers, Apache Arrow and Parquet).
Tiled enables slicing and sub-selection to read and transfer only the data of
interest, and it enables parallelized download of many chunks at once. Users can
access data with very light software dependencies and fast partial downloads.

Tiled puts an emphasis on **structures** rather than formats, including:

* N-dimensional strided arrays (i.e. numpy-like arrays)
* Sparse arrays
* Tabular data (e.g. pandas-like "dataframes")
* Nested, variable-sized data (as implemented by [AwkwardArray](https://awkward-array.org/))
* Hierarchical structures thereof (e.g. xarrays, HDF5-compatible structures like NeXus)

Tiled implements extensible **access control enforcement** based on web security
standards, similar to JuptyerHub. Like Jupyter, Tiled can be used by a single
user or deployed as a shared public or private resource. Tiled can be configured
to use third party services for login, such as Google, ORCID. or any OIDC
or SAML authentication providers.

Tiled facilitates **client-side caching** in a standard web browser or in
Tiled's Python client, making efficient use of bandwidth. It uses
**service-side caching** of "hot" datasets and resources to expedite both
repeat requests (e.g. when several users are requesting the same chunks of
data) and distinct requests for different parts of the same dataset (e.g. when
the user is requesting various slices or columns from a dataset).

| Distribution   | Where to get it                                              |
| -------------- | ------------------------------------------------------------ |
| PyPI           | `pip install tiled`                                          |
| Conda          | `conda install -c conda-forge tiled-client tiled-server`     |
| Source code    | [github.com/bluesky/tiled](https://github.com/bluesky/tiled) |
| Documentation  | [blueskyproject.io/tiled](https://blueskyproject.io/tiled)   |

## Example

In this example, we'll serve of a collection of data that is generated in
memory.  Alternatively, it could be read on demand from a directory of files,
network resource, database, or some combination of these.

```
tiled serve demo
# equivalent to:
# tiled serve pyobject --public tiled.examples.generated:tree
```

And then access the data efficiently via the Python client, a web browser, or
any HTTP client.

```python
>>> from tiled.client import from_uri

>>> client = from_uri("http://localhost:8000")

>>> client
<Container {'short_table', 'long_table', 'structured_data', ...} ~10 entries>

>>> list(client)
'big_image',
 'small_image',
 'tiny_image',
 'tiny_cube',
 'tiny_hypercube',
 'low_entropy',
 'high_entropy',
 'short_table',
 'long_table',
 'labeled_data',
 'structured_data']

>>> client['medium_image']
<ArrayClient>

>>> client['medium_image'][:]
array([[0.49675483, 0.37832119, 0.59431287, ..., 0.16990737, 0.5396537 ,
        0.61913812],
       [0.97062498, 0.93776709, 0.81797714, ..., 0.96508877, 0.25208564,
        0.72982507],
       [0.87173234, 0.83127946, 0.91758202, ..., 0.50487542, 0.03052536,
        0.9625512 ],
       ...,
       [0.01884645, 0.33107071, 0.60018523, ..., 0.02268164, 0.46955907,
        0.37842628],
       [0.03405101, 0.77886243, 0.14856727, ..., 0.02484926, 0.03850398,
        0.39086524],
       [0.16567224, 0.1347261 , 0.48809697, ..., 0.55021249, 0.42324589,
        0.31440635]])

>>> client['long_table']
<DataFrameClient ['A', 'B', 'C']>

>>> client['long_table'].read()
              A         B         C
index
0      0.246920  0.493840  0.740759
1      0.326005  0.652009  0.978014
2      0.715418  1.430837  2.146255
3      0.425147  0.850294  1.275441
4      0.781036  1.562073  2.343109
...         ...       ...       ...
99995  0.515248  1.030495  1.545743
99996  0.639188  1.278376  1.917564
99997  0.269851  0.539702  0.809553
99998  0.566848  1.133695  1.700543
99999  0.101446  0.202892  0.304338

[100000 rows x 3 columns]

>>> client['long_table'].read(['A', 'B'])
              A         B
index
0      0.246920  0.493840
1      0.326005  0.652009
2      0.715418  1.430837
3      0.425147  0.850294
4      0.781036  1.562073
...         ...       ...
99995  0.515248  1.030495
99996  0.639188  1.278376
99997  0.269851  0.539702
99998  0.566848  1.133695
99999  0.101446  0.202892
```

Using an Internet browser or a command-line HTTP client like
[curl](https://curl.se/) or [httpie](https://httpie.io/) you can download the
data in whole or in efficiently-chunked parts in the format of your choice:

```
# Download tabular data as CSV
http://localhost:8000/api/v1/node/full/long_table?format=csv

# or XLSX (Excel)
http://localhost:8000/api/v1/node/full/long_table?format=xslx

# and subselect columns.
http://localhost:8000/api/v1/node/full/long_table?format=xslx&field=A&field=B

# View or download (2D) array data as PNG
http://localhost:8000/api/v1/array/full/medium_image?format=png

# and slice regions of interest.
http://localhost:8000/api/v1/array/full/medium_image?format=png&slice=:50,100:200
```

Web-based data access usually involves downloading complete files, in the
manner of [Globus](https://www.globus.org/); or using modern chunk-based
storage formats, such as [TileDB](https://tiledb.com/) and
[Zarr](https://zarr.readthedocs.io/en/stable/) in local or cloud storage; or
using custom solutions tailored to a particular large dataset. Waiting for an
entire file to download when only the first frame of an image stack or a
certain column of a table are of interest is wasteful and can be prohibitive
for large longitudinal analyses. Yet, it is not always practical to transcode
the data into a chunk-friendly format or build a custom tile-based-access
solution. (Though if you can do either of those things, you should consider
them instead!)
