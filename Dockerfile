# Bluesky Base Dockerfile

##### shared environment stage #################################################

# Use Ubuntu 20.04 as the base image.
#FROM ubuntu:20.04 AS base
FROM python:3.10-slim-bullseye

##### developer / build stage ##################################################

# Install essential build tools and utilities.
RUN apt update -y && apt-get upgrade -y
RUN apt install -y --no-install-recommends \
    git \
    python3 \
    python3-pip \
    re2c \
    rsync \
    ssh-client \
    nano

RUN apt install -y libgl-dev libxkbcommon-dev libegl-dev libfontconfig-dev libdbus-1-dev \
    libxcb-*-dev libxkbcommon-x11-dev 
    
RUN apt install -y libzbar-dev
RUN pip install --upgrade pip
RUN python3 -m pip install PyQt5

# Install Python packages required for Bluesky and scientific computing.
RUN python3 -m pip install --upgrade bluesky ophyd matplotlib ipython pyepics
# RUN python3 -m pip install Levenshtein lmfit jsonschema==3.2 scikit-image
RUN python3 -m pip install Levenshtein lmfit jsonschema scikit-image
# Install tiled 
RUN python3 -m pip install --pre databroker[all]
# Install other packages like galvani
RUN python3 -m pip install galvani 

# Clone the 'beamline_test' Git repository into the 'bluesky' directory.
RUN cd /opt && git clone --recursive https://codebase.helmholtz.cloud/hzb/bluesky/containers/bluesky_beamlines/bluesky-hzb-collection.git bluesky

# Install the packages downloaded in bluesky
#RUN cd /opt/bluesky/beamlinetools && python3 -m pip install .
RUN cd /opt/bluesky/bessyii && python3 -m pip install .
RUN cd /opt/bluesky/bessyii_devices && python3 -m pip install .
RUN cd /opt/bluesky/suitcase-specfile && python3 -m pip install .

# Clone the 'shell-scripts-container' Git repository into the '/opt' directory.
RUN cd /opt && git clone https://codebase.helmholtz.cloud/hzb/bluesky/containers/bluesky_beamlines/shell-scripts-for-container.git

# Set the PATH environment variable to include '/opt/shell-scripts-container/bin'.
ENV PATH="/opt/shell-scripts-for-container/bin:$PATH"

# Create a directory structure and clone 'profile_root' Git repository.
RUN mkdir -p /opt/bluesky/ipython && cd /opt/bluesky/ipython && git clone https://codebase.helmholtz.cloud/hzb/bluesky/containers/bluesky_beamlines/profile-root-for-container.git profile_root

# Set the working directory to '/bluesky/'.
WORKDIR /opt/bluesky/


